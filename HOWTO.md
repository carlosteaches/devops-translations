# HOWTO

## Software Requirements
- youtube-dl
- subtitle edit (freeware from Microsoft Store)
- website https://www.subtitlevideo.com/youtube-subtitle-extractor/ 
- git or gitlab webUI

## QuickStart
- install [youtube-dl](https://github.com/ytdl-org/youtube-dl/blob/master/README.md#readme)
- install a subtitle editor
- download the subtitles for the video you want to translate
- rename subtitle file to have the same name of the youtube video
- add original subtitle file to repository
- search the [ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) for your target language
- start translating
- save new subtitle file to repository, using the language code to make the filename different from the source file
- iterate until you finish your translation
